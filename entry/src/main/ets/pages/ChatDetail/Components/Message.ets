import { WeChat_CurrentUserKey } from '../../../constant'
import { MessageInfo, MessageInfoModel, MessageTypeEnum } from '../../../models/message'
import { PopupItem } from '../../../models/popup'
import { UserInfo, UserInfoModel } from '../../../models/user'
import { AudioRenderer } from '../../../utils/audio_renderer'
import { FileCommon } from '../../../utils/file_operate'
import { VoiceTransfer } from '../../../utils/voice_transfer'

@Preview
@Component
struct Message {
  videoController: VideoController = new VideoController

  @StorageProp(WeChat_CurrentUserKey)//当前用户
  currentUser: UserInfoModel = new UserInfoModel({} as UserInfo)
  @Require
  @Prop//当前消息的内容
  currentMessage: MessageInfoModel = new MessageInfoModel({} as MessageInfo)
  @State
  isOwnMessage: boolean = this.currentUser.user_id === this.currentMessage?.sendUser?.user_id//是我发的消息,可选的，如果没有消息，也不至于因返回undefined而报错

  @State
  showPopup: boolean = false//弹层是否显示

  delMessage: (messId: string) => void = () => {}

  previewImg: () => void = () => {}//预览方法

  @State
  transferResult: string = ""//用来记录语音变量转化文字

  @State
  popupList: PopupItem[] = [{
    title: '听筒播放',
    icon: $r("app.media.ic_public_ears")
    },
    {
      title: '收藏',
      icon: $r("app.media.ic_public_cube")
    }, {
      title: '转文字',
      icon: $r("app.media.ic_public_trans_text"),
      itemClick: () => {
        if(this.currentMessage.messageType === MessageTypeEnum.AUDIO){
          VoiceTransfer.VoiceToText(this.currentMessage.sourceFilePath, (res) => {
            this.transferResult = res.result//拿到转化的文字
          })
        }
      }
    }, {
      title: '删除',
      icon: $r("app.media.ic_public_cancel"),
      itemClick: () => {
        if(this.currentMessage.sourceFilePath) {
          FileCommon.delFilePath(this.currentMessage.messageContent)
        }
        this.delMessage(this.currentMessage.id)
      }
    }, {
      title: '多选',
      icon: $r("app.media.ic_public_multi_select")
    }, {
      title: '引用',
      icon: $r("app.media.ic_public_link")
    }, {
      title: '提醒',
      icon: $r("app.media.ic_public_warin")
    }]

  @State
  audioState: AnimationStatus = AnimationStatus.Initial

  @Builder
  getContent() {//显示弹层的内容
    GridRow({ columns:5 }){
      ForEach(this.popupList,(item: PopupItem) => {
        GridCol(){
          Column({ space: 6 }){
            Image(item.icon)
              .fillColor($r('app.color.white'))
              .width(18)
              .aspectRatio(1)
            Text(item.title)
              .fontColor($r('app.color.white'))
              .fontSize(14)
          }
          .height(60)
        }
        .onClick(() => {
          item.itemClick && item.itemClick()//如果点击事件存在就触发
        })
      })
    }
    .padding({
      top: 15,
      left: 10
    })
    .width(300)
  }

  @Builder getTextContent () {
    Text(this.currentMessage.messageContent)
      .backgroundColor(this.isOwnMessage ? $r('app.color.second_primary') : $r('app.color.white'))
      .fontColor($r('app.color.text_primary'))
      .padding(10)
      .margin({
        left: 10,
        right: 10
      })
      .borderRadius(4)
  }

  getAudioWidth () {//获取语音条的时长
    let minWidth: number = 20//百分比
    let maxWidth: number = 90
    let calcWidth = minWidth + this.currentMessage.sourceDuration / 60 * 100
    if (calcWidth > maxWidth){
      return maxWidth+'%'
    }
    return calcWidth+'%'
  }

  @Builder getAudioContent () {
    Column() {
      Row({ space : 5 }){
        Text(`${this.currentMessage.sourceDuration}"`)
        ImageAnimator()
          .images([
            {
              src: $r('app.media.ic_public_voice3')
            },
            {
              src: $r('app.media.ic_public_voice1')
            },
            {
              src: $r('app.media.ic_public_voice2')
            },
            {
              src: $r('app.media.ic_public_voice3')
            },
          ])
          .state(this.audioState)
          .iterations(-1)//永远在播
          .width(20)
          .aspectRatio(1)
          .rotate({
            angle: this.isOwnMessage ? 180 : 0
          })
      }
      .justifyContent(this.isOwnMessage ? FlexAlign.End : FlexAlign.Start)
      .direction(this.isOwnMessage ? Direction.Ltr : Direction.Rtl)
      .width(this.getAudioWidth())
      .backgroundColor(this.isOwnMessage ? $r('app.color.second_primary') : $r('app.color.white'))
      .padding(10)
      .margin({
        left: 10,
        right: 10
      })
      .borderRadius(4)
      .onClick(() => {
        AudioRenderer.start(this.currentMessage.sourceFilePath,() => {
          this.audioState = AnimationStatus.Stopped
        })//播放
        this.audioState = AnimationStatus.Running
      })
      if(this.transferResult){
        Text(this.transferResult)
          .backgroundColor(this.isOwnMessage ? $r('app.color.second_primary') : $r('app.color.white'))
          .fontColor($r('app.color.text_primary'))
          .padding(10)
          .margin({
            left: 10,
            right: 10
          })
          .borderRadius(4)
      }
    }
    .alignItems(this.isOwnMessage ? HorizontalAlign.End : HorizontalAlign.Start)
  }

  @Builder
  getImageContent () {
    Column(){
      Image('file://' + this.currentMessage.sourceFilePath)//沙箱目录要显示在image上必须加文件协议
        .borderRadius(4)
        .width('100%')//高度不给，让图片自己撑
        .enabled(false)//有时候长按图片不能删除，因为图片本身在鸿蒙系统中有自己的动画效果，这和长按手势有冲突，把动画效果禁用
    }
    .width('40%')
    .margin({
      left: 10,
      right: 10
    })
    .onClick(() => {
      this.previewImg()//预览图片
    })
  }

  @Builder
  getVideoContent () {
    Column(){
      Stack(){
        Video({
          src: 'file"//' + this.currentMessage.sourceFilePath,
          controller: this.videoController
        })
          .width(100)
          .height(150)
          .borderRadius(4)
          .controls(false)
          .onPrepared(() => {
            this.videoController.setCurrentTime(0.1)//拨到0.1的位置，出现首帧图片
          })
        Image($r('app.media.ic_public_play'))
          .width(30)
          .aspectRatio(1)
          .fillColor($r('app.color.back_color'))
      }
    }
    .margin({
      left: 10,
      right: 10
    })
    .onClick(() => {
      this.previewImg()//预览视频
    })
  }

  build() {
    Row(){
      Image(this.currentMessage.sendUser.avatar)
        .width(40)
        .aspectRatio(1)
        .borderRadius(4)
      Row(){
        Column(){
          if(this.currentMessage.messageType === MessageTypeEnum.TEXT){
            this.getTextContent()
          }
          if(this.currentMessage.messageType === MessageTypeEnum.AUDIO){
            this.getAudioContent()
          }
          if(this.currentMessage.messageType === MessageTypeEnum.IMAGE){
            this.getImageContent()
          }
          if(this.currentMessage.messageType === MessageTypeEnum.VIDEO){
            this.getVideoContent()
          }
        }
        .bindPopup(this.showPopup, {
          builder: this.getContent(),
          popupColor: $r('app.color.popup_back'),
          backgroundBlurStyle: BlurStyle.NONE,//控制背景颜色不失焦
          onStateChange: (event) => {
            this.showPopup = event.isVisible//是不是弹窗正在显示
          }
        })
      }
      .justifyContent(this.isOwnMessage ? FlexAlign.End : FlexAlign.Start)
      .layoutWeight(6)
      .gesture(//手势的监听
        LongPressGesture()//长按
          .onAction(() => {//触发
            this.showPopup = true
          })
      )
      Text()
        .layoutWeight(1)
    }
    .padding({
      left: 20,
      right: 20
    })
    .alignItems(VerticalAlign.Top)
    .direction(this.isOwnMessage ? Direction.Rtl : Direction.Ltr)
  }
}
export default Message;