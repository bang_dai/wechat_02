import { UserInfo, UserInfoModel } from './user'
import { util } from '@kit.ArkTS'

export interface MessageInfo {
  id: string //消息的标识
  sendUser: UserInfo //该条消息的发送者
  connectUser: UserInfo //该条消息的接收者
  messageContent: string //消息内容:文本信息，【图片】，【视频】
  sendTime: number //发送时间
  messageType: MessageTypeEnum  //消息类型
  sourceFilePath: string //音频地址，图片地址，视频地址（不能存放在首选项中，首选项中只能最大放8kb，只能放在沙箱中）
  sourceDuration: number //音频或视频的长度
}

export enum MessageTypeEnum {
  TEXT, //文本
  IMAGE, //图片
  AUDIO, //音频
  VIDEO, //视频
  LOCATION, //位置
  LINK, //链接
}
export class MessageInfoModel implements MessageInfo {
  id: string = ''
  sendUser: UserInfo = new UserInfoModel({} as UserInfo)
  connectUser: UserInfo = new UserInfoModel({} as UserInfo)
  messageContent: string = ''
  sendTime: number = 0
  messageType: MessageTypeEnum = MessageTypeEnum.TEXT
  sourceFilePath: string = ''
  sourceDuration: number = 0

  constructor(model: MessageInfo) {
    this.id = model.id || util.generateRandomUUID()//如果不传id，生成userid
    this.sendUser = model.sendUser
    this.connectUser = model.connectUser
    this.messageContent = model.messageContent
    this.sendTime = model.sendTime || Date.now()//如果不传时间，默认发送时间
    this.messageType = model.messageType || MessageTypeEnum.TEXT//如果不传类型，默认文本类型
    this.sourceFilePath = model.sourceFilePath
    this.sourceDuration = model.sourceDuration
  }
}
